// @(#) types.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SYS_TYPES_H
#define SYS_TYPES_H

#ifndef SYS_INT_H

typedef unsigned int uint8_t __attribute__((__mode__(__QI__)));
typedef unsigned int uint16_t __attribute__((__mode__(__HI__)));
typedef unsigned int uint32_t __attribute__((__mode__(__SI__)));
typedef unsigned int uint64_t __attribute__((__mode__(__DI__)));

typedef _Bool i1;

typedef uint32_t uintptr_t;
typedef long unsigned int size_t;

_Static_assert(sizeof(uint8_t)  == 1, "uint8_t must be 1 byte wide");
_Static_assert(sizeof(uint16_t) == 2, "uint16_t must be 2 bytes wide");
_Static_assert(sizeof(uint32_t) == 4, "uint32_t must be 4 bytes wide");
_Static_assert(sizeof(uint64_t) == 8, "uint64_t must be 8 bytes wide");

#endif

#endif /* SYS_TYPES_H */