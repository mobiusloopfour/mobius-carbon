// @(#) time.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SYS_TIME_H
#define SYS_TIME_H

#include "types.h"
#include <machine/arch.h>

void __kern_delay(volatile size_t count);
#define delay __kern_delay

#define MPP_MASK (3 << 11)
#define MPP_M (3 << 11)
#define MPP_S (1 << 11)
#define MPP_U (0 << 11)
#define MIE (1 << 3)
#define MIE_MEIE (1 << 11) // extern
#define MIE_MTIE (1 << 7)  // timer
#define MIE_MSIE (1 << 3)  // sw
#define NCPU 8             // maximum number of CPUs
#define CLINT 0x2000000
#define CLINT_MTIMECMP(hartid) (CLINT + 0x4000 + 4 * (hartid))
#define CLINT_MTIME (CLINT + 0xBFF8) // cycles since boot.

extern void __kern_sys_timer();
extern void timer_init();

extern void __kern_delay(volatile size_t count);
#define delay __kern_delay

extern reg_t __kern_core_id();
#define core_id __kern_core_id

extern reg_t __kern_rmstatus();
#define rmstatus __kern_rmstatus

extern void __kern_wmstatus(reg_t r);
#define wmstatus __kern_wmstatus

extern void __kern_wmpec(reg_t r);
#define wmpec __kern_wmpec

extern void __kern_wmscratch(reg_t r);
#define wmscratch __kern_wmscratch

extern void __kern_mtvec(reg_t r);
#define mtvec __kern_mtvec

extern reg_t __kern_rmie();
#define rmie __kern_rmie

extern void __kern_wmie(reg_t r);
#define wmie __kern_wmie

extern void __kern_timer_handler();
#define timer_handler __kern_timer_handler

#endif /* !SYS_TIME_H */