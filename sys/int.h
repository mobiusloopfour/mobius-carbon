// @(#) int.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SYS_INT_H
#define SYS_INT_H

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
#if __SIZEOF_LONG__ == 8
typedef long int64_t;
typedef unsigned long uint64_t;
#elif __SIZEOF_LONG__ == 4
__extension__ typedef long long int64_t;
__extension__ typedef unsigned long long uint64_t;
#else
#error unsupported long size_t
#endif

typedef int8_t int_least8_t;
typedef int16_t int_least16_t;
typedef int32_t int_least32_t;
typedef int64_t int_least64_t;
typedef int64_t intmax_t;
typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;
typedef uint32_t uint_least32_t;
typedef uint64_t uint_least64_t;
typedef uint64_t uintmax_t;

typedef int32_t intptr_t;
typedef int32_t intfptr_t;
typedef uint32_t uintptr_t;
typedef uint32_t uintfptr_t;
typedef uint32_t vm_offset_t;
typedef uint32_t vm_size_t;

#if __SIZEOF_SIZE_T__ == 8
typedef long unsigned int size_t; /* sizeof() */
typedef int64_t ssize_t; /* byte count or error */
#elif __SIZEOF_SIZE_T__ == 4
typedef long unsigned int size_t;   /* sizeof() */
typedef long signed int ssize_t;   /* byte count or error */
#else
#error unsupported size_t size_t
#endif

#if __SIZEOF_PTRDIFF_T__ == 8
typedef int64_t ptrdiff_t; /* ptr1 - ptr2 */
#elif __SIZEOF_PTRDIFF_T__ == 4
typedef int32_t ptrdiff_t; /* ptr1 - ptr2 */
#else
#error unsupported ptrdiff_t size_t
#endif

/*
 * Target-dependent type definitions.
 */
#define __NO_STRICT_ALIGNMENT

/*
 * Standard type definitions.
 */
#ifdef __LP64__

#ifndef _STANDALONE
typedef double __double_t;
typedef float __float_t;
#endif
#else
typedef unsigned long clock_t;
typedef int32_t critical_t;
#ifndef _STANDALONE
typedef long double double_t;
typedef long double float_t;
#endif
#endif
typedef int32_t int_fast8_t;
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef int64_t int_fast64_t;
#ifdef __LP64__
typedef int64_t register_t;
typedef int64_t segsz_t; /* segment size_t (in pages) */
typedef int64_t time_t;  /* time()... */
#else
typedef int32_t register_t;
typedef int32_t segsz_t;
typedef int32_t time_t;
#endif
typedef uint32_t uint_fast8_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
typedef uint64_t uint_fast64_t;
#ifdef __LP64__
typedef uint64_t u_register_t;
typedef uint64_t vm_paddr_t;
#else
typedef uint32_t u_register_t;
typedef uint64_t vm_paddr_t;
#endif
typedef int wchar_t;

typedef int ct_rune_t;      /* arg type for ctype funcs */
typedef ct_rune_t rune_t; /* rune_t (see above) */
typedef ct_rune_t wint_t; /* wint_t (see above) */

#endif /* !SYS_INT_H */