// @(#) arg.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SYS_ARG_H
#define SYS_ARG_H

#ifndef _VA_LIST
typedef __builtin_va_list va_list;
#define _VA_LIST
#endif
#define va_start(ap, param) __builtin_va_start(ap, param)
#define va_end(ap) __builtin_va_end(ap)
#define va_arg(ap, type) __builtin_va_arg(ap, type)

#define __va_copy(d, s) __builtin_va_copy(d, s)

#if __STDC_VERSION__ >= 199901L || __cplusplus >= 201103L || !defined(__STRICT_ANSI__)
#define va_copy(dest, src) __builtin_va_copy(dest, src)
#endif

#ifndef __GNUC_VA_LIST
#define __GNUC_VA_LIST 1
typedef __builtin_va_list __gnuc_va_list;
#endif

#endif /* !SYS_ARG_H */