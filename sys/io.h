// @(#) io.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SYS_IO_H
#define SYS_IO_H

#include <sys/uart.h>
#include <sys/printf.h>

inline int kerror(char str[]) {
  return printf_("\033[1m\033[31m[/]\033[0m\033[31m %s\n\r\033[0m", str);
}

inline int ktrace(char str[]) {
  return printf_("\033[1m\033[32m[*]\033[0m\033[32m %s\n\r\033[0m", str);
}

#endif /* !SYS_IO_H */