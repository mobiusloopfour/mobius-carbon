// @(#) uart.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SYS_UART_H
#define SYS_UART_H

#define UART 0x10000000
#define UART_THR                                                               \
  (unsigned char *)(UART + 0x00) // THR:transmitter holding register
#define UART_LSR (unsigned char *)(UART + 0x05) // LSR:line status register
#define UART_LSR_EMPTY_MASK                                                    \
  0x40 // LSR Bit 6: Transmitter empty; both the THR and LSR are empty

int __kern_putc(char ch);
void __kern_puts(char str[]);

#define putc __kern_putc
#define puts __kern_puts

#endif /* !SYS_UART_H */