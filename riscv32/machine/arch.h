// @(#) machine/arch.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef RISCV32_MACHINE_ARCH_H
#define RISCV32_MACHINE_ARCH_H

typedef unsigned int reg_t;

typedef struct context {
    reg_t ra;
    reg_t sp;

    // callee-saved
    reg_t s0;
    reg_t s1;
    reg_t s2;
    reg_t s3;
    reg_t s4;
    reg_t s5;
    reg_t s6;
    reg_t s7;
    reg_t s8;
    reg_t s9;
    reg_t s10;
    reg_t s11;

} context_t;

#endif /* !RISCV32_MACHINE_ARCH_H */

// eof