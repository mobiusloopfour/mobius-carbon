// #(@) sched_proc.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <bogus/bogus.h>
#include <machine/arch.h>
#include <sched/proc.h>
#include <sched/switch.h>
#include <sys/types.h>

uint64_t __sched_top_task = 0;
uint8_t __sched_task_stk[MAX_TASKS][STACK_SIZE];

context_t __sched_ctx_tasks[MAX_TASKS];
context_t __sched_osctx;
context_t * __sched_cctx; // uintptr_t to current context

uint64_t __sched_mk_task(void (*task)(void)) {
    uint64_t i              = top_task++;
    __sched_ctx_tasks[i].ra = (reg_t)task;
    __sched_ctx_tasks[i].sp = (reg_t)&__sched_task_stk[i][STACK_SIZE - 1];

    return i;
}

void __sched_init_task(uint64_t i) {
    __sched_cctx = &__sched_ctx_tasks[i];
    __sched_swtch(&__sched_osctx, &__sched_ctx_tasks[i]);
}

void __sched_ret_to_k() {
    context_t * ctx = __sched_cctx;
    __sched_cctx    = &__sched_osctx;
    __sched_swtch(ctx, &__sched_osctx);
}