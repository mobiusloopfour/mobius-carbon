// @(#) proc.h

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or 
    modify it under the terms of the GNU General Public License; 
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but 
    WITHOUT ANY WARRANTY; without even the implied warranty of 
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#ifndef SCHED_PROC_H
#define SCHED_PROC_H

#include <sys/types.h>

#define STACK_SIZE 0x600

extern uint64_t __sched_top_task;
#define top_task __sched_top_task

// __sched_mk_task, __sched_init_task in a nice wrapper
extern uint64_t __sched_spawn(void (*task)(void));

extern uint64_t __sched_mk_task(void (*task)(void));
extern void __sched_init_task(uint64_t i);
extern void __sched_ret_to_k();

#define mk_task __sched_mk_task
#define init_task __sched_init_task
#define ret_to_k __sched_ret_to_k
#define spawn __sched_spawn

#endif /* !SCHED_PROC_H */