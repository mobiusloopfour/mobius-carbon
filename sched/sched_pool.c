// #(@) sched_pool.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <sched/proc.h>
#include <sched/switch.h>
#include <sys/io.h>
#include <sys/time.h>

void __sched_init_pool() {
    int current_task = 0;
    while (1) {
        __sched_init_task(current_task);
        current_task = (current_task + 1) % __sched_top_task;
    }

    // reachable with exceptions later on?
    kerror("Scheduler crashed");
    while (1)
        ;
}