# Carbon

Carbon is a small hobby microkernel.

# Features

- Context switching
- Pre-emptive multitasking capabilites
- User mode round-robin scheduler
- Simple, easy to understand code

## Todo:

- Mutex/spinlock api's
- Virtual memory management
- Virtual file system, with drivers for USTAR
- Graphical output
- A set of user space servers

# Building Carbon

## Linux, *BSD, Darwin, et al.

### Dependencies:

- clang 13
- llvm binutils (llvm-objdump, ld.lld, ...)
- make
- qemu-system-riscv32 and/or qemu-system-riscv64

### Instructions:

Make sure eveything is in your $PATH, and simply run

```bash
make            #build
make qemu       #run, outputs to serial port
```

# Contributing

Contributions are always welcome! If you think you've spotted a bug, submit a PR. If you've thought of a new feature, add a directory called `contrib/<feature or developer id>`. This is such that any new commits from me do not interfere.

Note on developer id's: these should be Java-styled reverse domains, such as `org.MobiusCarbon.Kernel`. If you don't have a domain name (such as me), you can put anything you like, but keep the three-part structure of `TOP_LEVEL`.`ID`.`PROJECT`. 

## Style

clang-format is ran when building the project, this should fix most issues. The naming convention is `snake_case`