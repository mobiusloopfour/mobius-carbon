ARCH ?= riscv32
OPTLEVEL ?= -Ofast
PREFIX ?= .

CC := clang -target $(ARCH)-none-elf -march=rv32ima -mabi=ilp32 -m32
CFLAGS := -nostdlib -fno-builtin -mcmodel=medany -fno-omit-frame-pointer 
CFLAGS += -fno-common -ffreestanding -fno-stack-protector
CFLAGS += $(OPTLEVEL)
CFLAGS += -I. -I$(ARCH) -Wall -Wextra -pipe -mno-relax -MMD

LD := ld.lld

QEMU := qemu-system-$(ARCH)
QFLAGS := -smp 4 -machine virt -bios none -nographic

OBJDUMP = llvm-objdump

S=$(ARCH)
C=.
KNAME=$(PREFIX)/krnl

OBJ := \
	$C/kern/kern_main.o \
	$C/kern/kern_uart.o \
	$C/kern/kern_time.o \
	$C/kern/kern_announce.o \
	$C/kern/kern_user_init.o \
	$C/sched/sched_pool.o \
	$C/sched/sched_proc.o \
	$C/lib/lib_math.o \
	$C/kern/kern_printf.o \
	$C/kern/kern_trap.o \
	$S/time.o \
	$S/traps.o \
	$S/start.o \
	$S/switch.o \

HDEPS := $(OBJ:.o=.d)

all: $(KNAME)

# $(CC) $(CFLAGS) -T krnl.ld -L lib/libgcc.a -L lib/libgcov.a -o $(KNAME) $^

$(KNAME): $(OBJ)
	$(LD) -Tkrnl.ld -o $@ $^
	$(OBJDUMP) -S $@ > _$@.s
	$(OBJDUMP) -t $@ | sed '1,/SYMBOL TABLE/d; s/ .* / /; /^$$/d' > _$@.dsym

clean:
	-@(rm krnl.sw $(OBJ) $(HDEPS) &> /dev/null) || true

distclean:
	-@(rm krnl.sw $(OBJ) $(HDEPS) *.dsym *.s &> /dev/null) || true

help:
	@echo "Have: clean, distclean, all <default>, qemu"
	@echo "Variables: ARCH - target triple, OPTLEVEL - -O0, -O1, -O2, -O3, -Ofast, -Os"

qemu: $(TARGET)
	@qemu-system-$(ARCH) -M ? | grep virt >/dev/null || exit
	$(QEMU) $(QFLAGS) -kernel $(KNAME)


-include $(HDEPS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
	clang-format -i $<

%.o: %.S
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY : all qemu clean