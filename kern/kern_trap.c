// #(@) kern_trap.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <machine/arch.h>
#include <sched/proc.h>
#include <sys/io.h>
#include <sys/time.h>

extern void __kern_trap_vector();

void trap_init() {
    // set the machine-mode trap handler.
    mtvec((reg_t)__kern_trap_vector);
    // enable machine-mode interrupts.
    wmstatus(rmstatus() | MIE);
}

reg_t __kern_trap_handler(reg_t epc, reg_t cause) {
    reg_t return_pc  = epc;
    reg_t cause_code = cause & 0xfff;

    if (cause & 0x80000000) {
        switch (cause_code) {
        case 3:
            // swi
            break;
        case 7:
            // timer int
            wmie(~((~rmie()) | (1 << 7)));
            timer_handler();
            return_pc = (reg_t)&__sched_ret_to_k;
            // enable machine-mode timer interrupts.
            wmie(rmie() | MIE_MTIE);
            break;
        case 11:
            // ext interrupt
            break;
        default:
            kerror("Unhandled interrupt");
            printf_(" -> %d\n", cause_code);
            break;
        }
    } else {
        // ya done fucked up
        printf_("\n");
        kerror("Exception triggered");
        printf_(" -> %d\n", cause_code);
        while (1)
            ;
    }
    return return_pc;
}