// #(@) kern_uart.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <sys/uart.h>

int __kern_putc(char ch) {
    while ((*UART_LSR & UART_LSR_EMPTY_MASK) == 0)
        ;
    return *UART_THR = ch;
}

void __kern_puts(char str[]) {
    while (*str)
        __kern_putc(*str++);
}

// eof