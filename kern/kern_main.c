// #(@) kern_main.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <bogus/bogus.h>
#include <machine/arch.h>
#include <sched/pool.h>
#include <sched/switch.h>
#include <sched/user.h>
#include <sys/io.h>
#include <sys/time.h>
#include <sys/types.h>

extern void trap_init();

uint32_t kmain(void) {
    announce(); // print some bs on the screen

    trap_init();
    timer_init(); // init timer

    user_init(); // load launch process(es)
    ktrace("Initialized");

    kerror("Unreachable state reached");

    return 0;
}

// eof