// #(@) kern_time.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <machine/arch.h>
#include <sys/io.h>
#include <sys/time.h>
#include <sys/types.h>

void __kern_delay(volatile size_t count) {
    count *= 50000;
    while (count--)
        ;
}

/* get the core */
reg_t __kern_core_id() {
    reg_t r;
    __asm__ volatile("csrr %0, mhartid" : "=r"(r));
    return r;
}

reg_t __kern_rmstatus() {
    reg_t r;
    __asm__ volatile("csrr %0, mstatus" : "=r"(r));
    return r;
}

void __kern_wmstatus(reg_t x) {
    __asm__ volatile("csrw mstatus, %0" : : "r"(x));
}

void __kern_wmpec(reg_t r) { __asm__ volatile("csrw mepc, %0" : : "r"(r)); }

reg_t __kern_rmpec() {
    reg_t r;
    __asm__ volatile("csrr %0, mepc" : "=r"(r));
    return r;
}

void __kern_wmscratch(reg_t r) { asm volatile("csrw mscratch, %0" : : "r"(r)); }

void __kern_mtvec(reg_t x) { asm volatile("csrw mtvec, %0" : : "r"(x)); }

reg_t __kern_rmie() {
    reg_t r;
    asm volatile("csrr %0, mie" : "=r"(r));
    return r;
}

void __kern_wmie(reg_t r) { asm volatile("csrw mie, %0" : : "r"(r)); }

#define interval 0x500

reg_t timer_scratch[NCPU][5];

void timer_init() {
    int id                       = core_id();
    *(reg_t *)CLINT_MTIMECMP(id) = *(reg_t *)CLINT_MTIME + interval;
    reg_t * scratch              = &timer_scratch[id][0];
    scratch[3]                   = CLINT_MTIMECMP(id);
    scratch[4]                   = interval;
    wmscratch((reg_t)scratch);
    wmie(rmie() | MIE_MTIE);
}

static int timer_count = 0;

void __kern_timer_handler() {
    // printf("__kern_timer_handler: %d\n", timer_count);
    timer_count += 1;
    int id                       = core_id();
    *(reg_t *)CLINT_MTIMECMP(id) = *(reg_t *)CLINT_MTIME + interval;
}

// eof