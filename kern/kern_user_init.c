// #(@) kern_user_init.c

/*
    Copyright (C) 2021 MobiusLoopFour.

    This file is part of the carbon kernel.

    The carbon kernel is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License;
    either version 3, or (at your option) any later version.

    The carbon kernel is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with the carbon kernel; see the file COPYING.  If not see
    <http://www.gnu.org/licenses/>.
*/

#include <sched/proc.h>
#include <sys/time.h>

void dummy_task_first() { /*dummy task */
}
void dummy_task_last() { /*dummy task */
}

extern void __sched_init_pool();

void __kern_user_init() {
    mk_task(&dummy_task_first);

    /* ... add here ... (mk_task) */

    mk_task(&dummy_task_last);
    // init the scheduler
    __sched_init_task(__sched_mk_task(&__sched_init_pool));
}

// eof